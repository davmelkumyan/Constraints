﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constraints_Equals
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass<int, object, int> myClass = new MyClass<int, object, int>();
            //MyClass<int, object, string> myClass = new MyClass<int, object, string>();    Ошибка
        }
    }

    class MyClass<T, U, V> where T : V { } // Где Т должен быть типа V
}
