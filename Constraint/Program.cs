﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constraint
{
    class Program
    {
        // LIMITATION of GENERICS
        static void Main(string[] args)
        {
            MyClassRef<string> myClassRef = new MyClassRef<string>();
            //MyClassRef<int> myClassRef = new MyClassRef<int>();

            MyClassStruct<int> myClassStruct = new MyClassStruct<int>();
            //MyClassStruct<string> myClassStruct = new MyClassStruct<string>();
            
        }
    }

    class MyClassRef<T> where T : class 
    {

    }

    class MyClassStruct<T> where T : struct
    {

    }
}
