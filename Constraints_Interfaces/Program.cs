﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constraints_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            var myClassDoubleInterface = new MyClassDoubleInterface<Derived>();
            var myClassInterface = new MyClassInterface<Derived>();
            var myClassGenericIInterface = new MyClassGenericIInterface<Derived>();
        }
    }


    interface IInterface { }
    interface IInterface<T> { }

    class Derived : IInterface, IInterface<object>
    {

    }

    class MyClassDoubleInterface<T> where T : IInterface, IInterface<object>
    {

    } 

    class MyClassInterface<T> where T : IInterface
    {

    }

    class MyClassGenericIInterface<T> where T : IInterface<object>
    {

    }

}
