﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constraint_New__
{
    class Program
    {
        static void Main(string[] args)
        {
            var fff = new Myclass<TestClass>();
            fff.instance.IntValue = 5;
            fff.instance.StringValue = "egewge";
            fff.Meth();
        }
    }

    class Myclass<T>/*Если убрать ограничение то не возможно создать экземпляр в классе Generic*/ where T : new()
    {
       public T instance = new T();

        public void Meth()
        {
            Console.WriteLine(instance.ToString());
        }
    }

    class TestClass
    {
        public int IntValue { get; set; }
        public string StringValue { get; set; }

        public override string ToString()
        {
            return $" {IntValue}    {IntValue}";
        }
    }
}
