﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constraints_Base
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass<Base> myClassBase = new MyClass<Base>();
            MyClass<Derived> myClassDerived = new MyClass<Derived>();
        }
    }


    class Base { }
    class Derived : Base { }

    class MyClass<T> where T : Base
    {

    } 
}
