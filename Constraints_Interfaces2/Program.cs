﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constraints_Interfaces2
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass<Derived> my = new MyClass<Derived>();

            MyClass2<Derived2> my2 = new MyClass2<Derived2>();
        }
    }

    interface IInterface { }
    interface IInterface<T> : IInterface { }

    class Derived : IInterface
    {

    }

    class Derived2 : IInterface<object>
    {

    }

    class MyClass<T> where T : IInterface { }
    
    class MyClass2<T> where T : IInterface<object>, IInterface{ }
}
